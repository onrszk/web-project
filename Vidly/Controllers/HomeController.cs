﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;

namespace Vidly.Controllers
{
    public class HomeController : Controller
    {
    
        logindbEntities3 db = new logindbEntities3();
        
        public ActionResult Index()
        {
            if (Session["user"] == null) { return RedirectToAction("LogIn", "Home"); }
            else
            {
                var returnList = db.urunTbl.ToList();
                return View(returnList);
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "ŞİRKETE GENEL BAKIŞ";

            return View();
        }

        public ActionResult Contact()
        {
            //ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpGet]
        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogIn(Models.tblLogin userr)
        {
            
            if (ModelState.IsValid)
            {
                using (db)
                {
                    var v = db.tblLogin.Where(a => a.name.Equals(userr.name) && a.password.Equals(userr.password)).FirstOrDefault();
                
                    if (v != null)
                    {
                        Session["user"] = v.id.ToString();
                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            return View(userr);
        }
        [HttpGet]
        public ActionResult Create() {
            if (Session["user"] == null) { return RedirectToAction("LogIn", "Home"); }
            else return View();
        }
        [HttpPost]
        public ActionResult Create(urunTbl item) {
            if (Session["user"] == null) { return RedirectToAction("LogIn", "Home"); }
            db.urunTbl.Add(item);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            
            using (logindbEntities3 dbModel1 = new logindbEntities3())
            {
                return View(dbModel1.urunTbl.Where(x => x.id == id).FirstOrDefault());
            }
        }
        [HttpPost]
        public ActionResult Edit(int? id,urunTbl urun)
        {
             
            try
            {
                using (logindbEntities3 dbModel1 = new logindbEntities3())
                {
                    dbModel1.Entry(urun).State = EntityState.Modified;
                    dbModel1.SaveChanges();
                }
                    return RedirectToAction("Index");

            }
            catch
            {
                return View();
            }
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            using (logindbEntities3 dbModel1 = new logindbEntities3())
            {
                return View(dbModel1.urunTbl.Where(x => x.id == id).FirstOrDefault());
            }

        }

        [HttpPost]
        public ActionResult Delete(int id, urunTbl urun)
        {
            try
            {
                using (logindbEntities3 dbModel1 = new logindbEntities3())
                {
                    urunTbl urunn = dbModel1.urunTbl.Where(x => x.id == id).FirstOrDefault();
                    dbModel1.urunTbl.Remove(urunn);
                    dbModel1.SaveChanges();
                }
                return RedirectToAction("Index");

            }
            catch
            {
                return View();
            }
        }
    }
}