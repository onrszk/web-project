﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Vidly
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            //Normal yöntem. Eğer bir route bu şekilde yapılırsa Controller ' ındaki action ın değişmesi durumunda burdaki action değişmez.Manuel olarak değiştirmemiz gerekir.
            //routes.MapRoute(
            //    "MoviesByReleaseDate",
            //    "movies/released/{year}/{month}",
            //    new { controller = "Movies", action = "ByReleaseDate" },
            //    new { year = @"\d{4}",month=@"\d{2}" }
            //    );
            //    //year = @"2015|2016   olursa sadece 2015 ve 2016 değerlerini kabul eder.Diğerlerinde hata veririr.
                
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
